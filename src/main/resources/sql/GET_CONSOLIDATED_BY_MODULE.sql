SELECT ABS (SUM (AMOUNT)) AMOUNT,
       AccountNumber,
       AccountType,
       AccountModuleType,
       isExecutable,
       InitiatorBranchId,
       BranchId,
       CurrencyCode,
       (CASE WHEN SUM (AMOUNT) >= 0 THEN 'CREDIT' ELSE 'DEBIT' END) TYPE,
       TransactionRootId,
       BatchFileId,
       AmountType,
       TransactionDefinitionId
FROM (  SELECT (CASE
                  WHEN TXN.ACC_TYPE = 0
                          THEN
    (CASE
       WHEN TXN.TYPE = 'CREDIT'
               THEN
        SUM (TXN.AMOUNT) * (-1)
       ELSE
        SUM (TXN.AMOUNT)
        END)
                  ELSE
    (CASE
       WHEN TXN.TYPE = 'DEBIT'
               THEN
        SUM (TXN.AMOUNT) * (-1)
       ELSE
        SUM (TXN.AMOUNT)
        END)
    END)
                                                AMOUNT,
               TXN.ACC_NUM AS AccountNumber,
               TXN.ACC_TYPE AS AccountType,
               TXN.ACC_MODULE_TYPE AS AccountModuleType,
               TXN.IS_EXECUTABLE AS isExecutable,
               TXN.INITIATOR_BRANCH_ID AS InitiatorBranchId,
               TXN.BRANCH_ID AS BranchId,
               TXN.CURRENCY_CODE AS CurrencyCode,
               TXN.TYPE,
               TXN.TRANSACTION_ROOT_ID AS TransactionRootId,
               TXN.BATCH_FILE_ID AS BatchFileId,
               TXN.AMOUNT_TYPE AS AmountType,
               TXN.TRANSACTION_DEFINITION_ID AS TransactionDefinitionId
        FROM TXN_TRANSACTION_ACC_VALIDATED TXN,
             TXN_TRANSACTION_ROOT ROOT,
             TXN_BATCH_FILE FILES
        WHERE     ROOT.ID = TXN.TRANSACTION_ROOT_ID
          AND ROOT.IS_EXECUTABLE = 1
          AND TXN.IS_EXECUTABLE = 1
          AND ROOT.BATCH_FILE_ID = FILES.ID
          AND ROOT.BATCH_FILE_ID = :batchFileId
          AND TXN.ACC_MODULE_TYPE = :moduleType
          AND FILES.IS_CUMULATIVE_ALLOWED = 1
        GROUP BY TXN.ACC_NUM,
                 TXN.ACC_TYPE,
                 TXN.ACC_MODULE_TYPE,
                 TXN.BRANCH_ID,
                 TXN.CURRENCY_CODE,
                 TXN.TYPE,
                 TXN.TRANSACTION_ROOT_ID,
                 TXN.BATCH_FILE_ID,
                 TXN.IS_EXECUTABLE,
                 TXN.INITIATOR_BRANCH_ID,
                 FILES.IS_CUMULATIVE_ALLOWED,
                 TXN.AMOUNT_TYPE,
                 TXN.TRANSACTION_DEFINITION_ID)
GROUP BY AccountNumber,
         AccountType,
         AccountModuleType,
         isExecutable,
         InitiatorBranchId,
         BranchId,
         CurrencyCode,
         TransactionRootId,
         BatchFileId,
         AmountType,
         TransactionDefinitionId
UNION ALL
SELECT SUM (TXN.AMOUNT) AMOUNT,
       TXN.ACC_NUM AS AccountNumber,
       TXN.ACC_TYPE AS AccountType,
       TXN.ACC_MODULE_TYPE AS AccountModuleType,
       TXN.IS_EXECUTABLE AS isExecutable,
       TXN.INITIATOR_BRANCH_ID AS InitiatorBranchId,
       TXN.BRANCH_ID AS BranchId,
       TXN.CURRENCY_CODE AS CurrencyCode,
       TXN.TYPE,
       TXN.TRANSACTION_ROOT_ID AS TransactionRootId,
       TXN.BATCH_FILE_ID AS BatchFileId,
       TXN.AMOUNT_TYPE AS AmountType,
       TXN.TRANSACTION_DEFINITION_ID AS TransactionDefinitionId
FROM TXN_TRANSACTION_ACC_VALIDATED TXN,
     TXN_TRANSACTION_ROOT ROOT,
     TXN_BATCH_FILE FILES
WHERE     ROOT.ID = TXN.TRANSACTION_ROOT_ID
  AND ROOT.IS_EXECUTABLE = 1
  AND TXN.IS_EXECUTABLE = 1
  AND ROOT.BATCH_FILE_ID = FILES.ID
  AND ROOT.BATCH_FILE_ID = :batchFileId
  AND TXN.ACC_MODULE_TYPE = :moduleType
  AND FILES.IS_CUMULATIVE_ALLOWED = 0
GROUP BY TXN.ACC_NUM,
         TXN.ACC_TYPE,
         TXN.ACC_MODULE_TYPE,
         TXN.BRANCH_ID,
         TXN.CURRENCY_CODE,
         TXN.TYPE,
         TXN.TRANSACTION_ROOT_ID,
         TXN.BATCH_FILE_ID,
         TXN.IS_EXECUTABLE,
         TXN.INITIATOR_BRANCH_ID,
         TXN.AMOUNT_TYPE,
         TXN.TRANSACTION_DEFINITION_ID,
         FILES.IS_CUMULATIVE_ALLOWED