CREATE OR REPLACE PACKAGE BODY ABABIL_NG.BATCH_TXN_CORE
AS
   PROCEDURE DO_BATCH_TXN (PBATCHFILEID IN NUMBER)
   IS
      LGLOBALTXNNO         VARCHAR2 (255);
      LTRECERNO            NUMBER;
      LIS_EXECUTED         NUMBER;
      LRATE                NUMBER;
      LEXCHANGE_RATETYPE   VARCHAR2 (10);
      LBATCHNO             VARCHAR2 (255);
      LTRDEFID             NUMBER;
      LBATCHFILEID         NUMBER;
   BEGIN
      -- LGLOBALTXNNO := GET_GLOBALTXNNO ();
      LBATCHFILEID := PBATCHFILEID;


      --===================================LIABILITY_CREDIT====================================
      FOR LTXN_LIABILITY_CREDIT
         IN (SELECT *
               FROM TXN_TXN_BALANCE_VALIDATED txn
              WHERE     TXN.BATCH_FILE_ID = LBATCHFILEID
                    AND TXN.ACC_TYPE = 1
                    AND TXN.TYPE = 'CREDIT'
                    AND NOT EXISTS
                               (SELECT 1
                                  FROM TXN_TXN_BALANCE_VALIDATED txnb
                                 WHERE     TXNB.TRANSACTION_ROOT_ID =
                                              txn.TRANSACTION_ROOT_ID
                                       AND TXNB.IS_EXECUTABLE = 0))
      LOOP
         BEGIN
            LRATE :=
               GET_SYSTEM_EXCHANGE_RATE (LTXN_LIABILITY_CREDIT.CURRENCY_CODE);
            LEXCHANGE_RATETYPE :=
               GET_APPCONFIGURATION_VALUE ('SYSTEM_EXCHANGE_RATE_TYPE');
            ALL_TXN_CORE.GETAUTOGENBATCHNO (
               LTXN_LIABILITY_CREDIT.INITIATOR_BRANCH_ID,
               LBATCHNO);

            --            IF LTXN_LIABILITY_CREDIT.ACC_MODULE_TYPE = 'GL'
            --            THEN
            --               LTRDEFID :=
            --                  GET_TXN_DEFINITION_ID (0,
            --                                         2,
            --                                         'GL',
            --                                         'PRINCIPAL');
            --            ELSIF LTXN_LIABILITY_CREDIT.ACC_MODULE_TYPE = 'CASA'
            --            THEN
            --               LTRDEFID :=
            --                  GET_TXN_DEFINITION_ID (0,
            --                                         2,
            --                                         'CASA',
            --                                         'PRINCIPAL');
            --            END IF;


            ALL_TXN_CORE.DO_TRANSACTION (
               LTXN_LIABILITY_CREDIT.CREATE_DATE_TIME,
               LTXN_LIABILITY_CREDIT.CREATE_DATE_TIME,
               LTXN_LIABILITY_CREDIT.AMOUNT,
               LTXN_LIABILITY_CREDIT.AMOUNT,
               LRATE,
               LEXCHANGE_RATETYPE,
               LTXN_LIABILITY_CREDIT.CURRENCY_CODE,
               LTXN_LIABILITY_CREDIT.ACC_NUM,
               LTXN_LIABILITY_CREDIT.NARRATION,
               LBATCHNO,
               LTXN_LIABILITY_CREDIT.ID,
               0,
               NULL,
               LTXN_LIABILITY_CREDIT.ACC_MODULE_TYPE,
               NULL,
               NULL,
               NULL,
               LTXN_LIABILITY_CREDIT.TRANSACTION_DEFINITION_ID,
               LTXN_LIABILITY_CREDIT.BRANCH_ID,
               LTXN_LIABILITY_CREDIT.INITIATOR_BRANCH_ID,
               LTXN_LIABILITY_CREDIT.CREATED_BY,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               LTRECERNO,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL);
            LIS_EXECUTED := 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               COMMON.RAISEEXCEPTION (
                  SQLERRM || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         -- LIS_EXECUTED := 0;
         END;

         INSERT INTO TXN_TRANSACTION (ID,
                                      CREATE_DATE,
                                      CREATE_DATE_TIME,
                                      CREATE_TERMINAL,
                                      CREATED_BY,
                                      PROCESS_ID,
                                      UPDATE_DATE,
                                      UPDATE_DATE_TIME,
                                      UPDATE_TERMINAL,
                                      UPDATED_BY,
                                      UUID,
                                      VERIFY_BY,
                                      VERIFY_DATE,
                                      VERIFY_DATE_TIME,
                                      VERIFY_TERMINAL,
                                      ACC_MODULE_TYPE,
                                      ACC_NUM,
                                      ACC_TYPE,
                                      AMOUNT,
                                      BRANCH_ID,
                                      BRANCH_PREDEFINED_OPTION,
                                      CURRENCY_CODE,
                                      IS_EXECUTABLE,
                                      INITIATOR_BRANCH_ID,
                                      NARRATION,
                                      TYPE,
                                      IS_EXECUTED,
                                      BATCH_FILE_ID,
                                      TRANSACTION_ROOT_ID,
                                      AMOUNT_TYPE,
                                      TRANSACTION_DEFINITION_ID)
              VALUES (S_TXN_TRANSACTION.NEXTVAL,
                      LTXN_LIABILITY_CREDIT.CREATE_DATE,
                      LTXN_LIABILITY_CREDIT.CREATE_DATE_TIME,
                      LTXN_LIABILITY_CREDIT.CREATE_TERMINAL,
                      LTXN_LIABILITY_CREDIT.CREATED_BY,
                      LTXN_LIABILITY_CREDIT.PROCESS_ID,
                      LTXN_LIABILITY_CREDIT.UPDATE_DATE,
                      LTXN_LIABILITY_CREDIT.UPDATE_DATE_TIME,
                      LTXN_LIABILITY_CREDIT.UPDATE_TERMINAL,
                      LTXN_LIABILITY_CREDIT.UPDATED_BY,
                      LTXN_LIABILITY_CREDIT.UUID,
                      LTXN_LIABILITY_CREDIT.VERIFY_BY,
                      LTXN_LIABILITY_CREDIT.VERIFY_DATE,
                      LTXN_LIABILITY_CREDIT.VERIFY_DATE_TIME,
                      LTXN_LIABILITY_CREDIT.VERIFY_TERMINAL,
                      LTXN_LIABILITY_CREDIT.ACC_MODULE_TYPE,
                      LTXN_LIABILITY_CREDIT.ACC_NUM,
                      LTXN_LIABILITY_CREDIT.ACC_TYPE,
                      LTXN_LIABILITY_CREDIT.AMOUNT,
                      LTXN_LIABILITY_CREDIT.BRANCH_ID,
                      LTXN_LIABILITY_CREDIT.BRANCH_PREDEFINED_OPTION,
                      LTXN_LIABILITY_CREDIT.CURRENCY_CODE,
                      LTXN_LIABILITY_CREDIT.IS_EXECUTABLE,
                      LTXN_LIABILITY_CREDIT.INITIATOR_BRANCH_ID,
                      LTXN_LIABILITY_CREDIT.NARRATION,
                      LTXN_LIABILITY_CREDIT.TYPE,
                      LIS_EXECUTED,
                      LTXN_LIABILITY_CREDIT.BATCH_FILE_ID,
                      LTXN_LIABILITY_CREDIT.TRANSACTION_ROOT_ID,
                      LTXN_LIABILITY_CREDIT.AMOUNT_TYPE,
                      LTXN_LIABILITY_CREDIT.TRANSACTION_DEFINITION_ID);
      END LOOP;

      --====================================ASSET_DEBIT===================================
      FOR LTXN_ASSET_DEBIT
         IN (SELECT *
               FROM TXN_TXN_BALANCE_VALIDATED txn
              WHERE     TXN.BATCH_FILE_ID = LBATCHFILEID
                    AND TXN.ACC_TYPE = 0
                    AND TXN.TYPE = 'DEBIT'
                    AND NOT EXISTS
                               (SELECT 1
                                  FROM TXN_TXN_BALANCE_VALIDATED txnb
                                 WHERE     TXNB.TRANSACTION_ROOT_ID =
                                              txn.TRANSACTION_ROOT_ID
                                       AND TXNB.IS_EXECUTABLE = 0))
      LOOP
         BEGIN
            LRATE := GET_SYSTEM_EXCHANGE_RATE (LTXN_ASSET_DEBIT.CURRENCY_CODE);
            LEXCHANGE_RATETYPE :=
               GET_APPCONFIGURATION_VALUE ('SYSTEM_EXCHANGE_RATE_TYPE');
            ALL_TXN_CORE.GETAUTOGENBATCHNO (
               LTXN_ASSET_DEBIT.INITIATOR_BRANCH_ID,
               LBATCHNO);

            --            IF LTXN_ASSET_DEBIT.ACC_MODULE_TYPE = 'GL'
            --            THEN
            --               LTRDEFID :=
            --                  GET_TXN_DEFINITION_ID (1,
            --                                         2,
            --                                         'GL',
            --                                         'PRINCIPAL');
            --            ELSIF LTXN_ASSET_DEBIT.ACC_MODULE_TYPE = 'CASA'
            --            THEN
            --               LTRDEFID :=
            --                  GET_TXN_DEFINITION_ID (1,
            --                                         2,
            --                                         'CASA',
            --                                         'PRINCIPAL');
            --            END IF;

            ALL_TXN_CORE.DO_TRANSACTION (
               LTXN_ASSET_DEBIT.CREATE_DATE_TIME,
               LTXN_ASSET_DEBIT.CREATE_DATE_TIME,
               LTXN_ASSET_DEBIT.AMOUNT,
               LTXN_ASSET_DEBIT.AMOUNT,
               LRATE,
               LEXCHANGE_RATETYPE,
               LTXN_ASSET_DEBIT.CURRENCY_CODE,
               LTXN_ASSET_DEBIT.ACC_NUM,
               LTXN_ASSET_DEBIT.NARRATION,
               LBATCHNO,
               LTXN_ASSET_DEBIT.ID,
               0,
               NULL,
               LTXN_ASSET_DEBIT.ACC_MODULE_TYPE,
               NULL,
               NULL,
               NULL,
               LTXN_ASSET_DEBIT.TRANSACTION_DEFINITION_ID,
               LTXN_ASSET_DEBIT.BRANCH_ID,
               LTXN_ASSET_DEBIT.INITIATOR_BRANCH_ID,
               LTXN_ASSET_DEBIT.CREATED_BY,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               LTRECERNO,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL);
            LIS_EXECUTED := 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               COMMON.RAISEEXCEPTION (
                  SQLERRM || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         --LIS_EXECUTED := 0;
         END;

         INSERT INTO TXN_TRANSACTION (ID,
                                      CREATE_DATE,
                                      CREATE_DATE_TIME,
                                      CREATE_TERMINAL,
                                      CREATED_BY,
                                      PROCESS_ID,
                                      UPDATE_DATE,
                                      UPDATE_DATE_TIME,
                                      UPDATE_TERMINAL,
                                      UPDATED_BY,
                                      UUID,
                                      VERIFY_BY,
                                      VERIFY_DATE,
                                      VERIFY_DATE_TIME,
                                      VERIFY_TERMINAL,
                                      ACC_MODULE_TYPE,
                                      ACC_NUM,
                                      ACC_TYPE,
                                      AMOUNT,
                                      BRANCH_ID,
                                      BRANCH_PREDEFINED_OPTION,
                                      CURRENCY_CODE,
                                      IS_EXECUTABLE,
                                      INITIATOR_BRANCH_ID,
                                      NARRATION,
                                      TYPE,
                                      IS_EXECUTED,
                                      BATCH_FILE_ID,
                                      TRANSACTION_ROOT_ID,
                                      AMOUNT_TYPE,
                                      TRANSACTION_DEFINITION_ID)
              VALUES (S_TXN_TRANSACTION.NEXTVAL,
                      LTXN_ASSET_DEBIT.CREATE_DATE,
                      LTXN_ASSET_DEBIT.CREATE_DATE_TIME,
                      LTXN_ASSET_DEBIT.CREATE_TERMINAL,
                      LTXN_ASSET_DEBIT.CREATED_BY,
                      LTXN_ASSET_DEBIT.PROCESS_ID,
                      LTXN_ASSET_DEBIT.UPDATE_DATE,
                      LTXN_ASSET_DEBIT.UPDATE_DATE_TIME,
                      LTXN_ASSET_DEBIT.UPDATE_TERMINAL,
                      LTXN_ASSET_DEBIT.UPDATED_BY,
                      LTXN_ASSET_DEBIT.UUID,
                      LTXN_ASSET_DEBIT.VERIFY_BY,
                      LTXN_ASSET_DEBIT.VERIFY_DATE,
                      LTXN_ASSET_DEBIT.VERIFY_DATE_TIME,
                      LTXN_ASSET_DEBIT.VERIFY_TERMINAL,
                      LTXN_ASSET_DEBIT.ACC_MODULE_TYPE,
                      LTXN_ASSET_DEBIT.ACC_NUM,
                      LTXN_ASSET_DEBIT.ACC_TYPE,
                      LTXN_ASSET_DEBIT.AMOUNT,
                      LTXN_ASSET_DEBIT.BRANCH_ID,
                      LTXN_ASSET_DEBIT.BRANCH_PREDEFINED_OPTION,
                      LTXN_ASSET_DEBIT.CURRENCY_CODE,
                      LTXN_ASSET_DEBIT.IS_EXECUTABLE,
                      LTXN_ASSET_DEBIT.INITIATOR_BRANCH_ID,
                      LTXN_ASSET_DEBIT.NARRATION,
                      LTXN_ASSET_DEBIT.TYPE,
                      LIS_EXECUTED,
                      LTXN_ASSET_DEBIT.BATCH_FILE_ID,
                      LTXN_ASSET_DEBIT.TRANSACTION_ROOT_ID,
                      LTXN_ASSET_DEBIT.AMOUNT_TYPE,
                      LTXN_ASSET_DEBIT.TRANSACTION_DEFINITION_ID);
      END LOOP;

      --====================================ASSET_CREDIT===================================
      FOR LTXN_ASSET_CREDIT
         IN (SELECT *
               FROM TXN_TXN_BALANCE_VALIDATED txn
              WHERE     TXN.BATCH_FILE_ID = LBATCHFILEID
                    AND TXN.ACC_TYPE = 0
                    AND TXN.TYPE = 'CREDIT'
                    AND NOT EXISTS
                               (SELECT 1
                                  FROM TXN_TXN_BALANCE_VALIDATED txnb
                                 WHERE     TXNB.TRANSACTION_ROOT_ID =
                                              txn.TRANSACTION_ROOT_ID
                                       AND TXNB.IS_EXECUTABLE = 0))
      LOOP
         BEGIN
            LRATE :=
               GET_SYSTEM_EXCHANGE_RATE (LTXN_ASSET_CREDIT.CURRENCY_CODE);
            LEXCHANGE_RATETYPE :=
               GET_APPCONFIGURATION_VALUE ('SYSTEM_EXCHANGE_RATE_TYPE');
            ALL_TXN_CORE.GETAUTOGENBATCHNO (
               LTXN_ASSET_CREDIT.INITIATOR_BRANCH_ID,
               LBATCHNO);

            --            IF LTXN_ASSET_CREDIT.ACC_MODULE_TYPE = 'GL'
            --            THEN
            --               LTRDEFID :=
            --                  GET_TXN_DEFINITION_ID (0,
            --                                         2,
            --                                         'GL',
            --                                         'PRINCIPAL');
            --            ELSIF LTXN_ASSET_CREDIT.ACC_MODULE_TYPE = 'CASA'
            --            THEN
            --               LTRDEFID :=
            --                  GET_TXN_DEFINITION_ID (0,
            --                                         2,
            --                                         'CASA',
            --                                         'PRINCIPAL');
            --            END IF;

            ALL_TXN_CORE.DO_TRANSACTION (
               LTXN_ASSET_CREDIT.CREATE_DATE_TIME,
               LTXN_ASSET_CREDIT.CREATE_DATE_TIME,
               LTXN_ASSET_CREDIT.AMOUNT,
               LTXN_ASSET_CREDIT.AMOUNT,
               LRATE,
               LEXCHANGE_RATETYPE,
               LTXN_ASSET_CREDIT.CURRENCY_CODE,
               LTXN_ASSET_CREDIT.ACC_NUM,
               LTXN_ASSET_CREDIT.NARRATION,
               LBATCHNO,
               LTXN_ASSET_CREDIT.ID,
               0,
               NULL,
               LTXN_ASSET_CREDIT.ACC_MODULE_TYPE,
               NULL,
               NULL,
               NULL,
               LTXN_ASSET_CREDIT.TRANSACTION_DEFINITION_ID,
               LTXN_ASSET_CREDIT.BRANCH_ID,
               LTXN_ASSET_CREDIT.INITIATOR_BRANCH_ID,
               LTXN_ASSET_CREDIT.CREATED_BY,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               LTRECERNO,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL);
            LIS_EXECUTED := 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               COMMON.RAISEEXCEPTION (
                  SQLERRM || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         --               LIS_EXECUTED := 0;
         END;

         INSERT INTO TXN_TRANSACTION (ID,
                                      CREATE_DATE,
                                      CREATE_DATE_TIME,
                                      CREATE_TERMINAL,
                                      CREATED_BY,
                                      PROCESS_ID,
                                      UPDATE_DATE,
                                      UPDATE_DATE_TIME,
                                      UPDATE_TERMINAL,
                                      UPDATED_BY,
                                      UUID,
                                      VERIFY_BY,
                                      VERIFY_DATE,
                                      VERIFY_DATE_TIME,
                                      VERIFY_TERMINAL,
                                      ACC_MODULE_TYPE,
                                      ACC_NUM,
                                      ACC_TYPE,
                                      AMOUNT,
                                      BRANCH_ID,
                                      BRANCH_PREDEFINED_OPTION,
                                      CURRENCY_CODE,
                                      IS_EXECUTABLE,
                                      INITIATOR_BRANCH_ID,
                                      NARRATION,
                                      TYPE,
                                      IS_EXECUTED,
                                      BATCH_FILE_ID,
                                      TRANSACTION_ROOT_ID,
                                      AMOUNT_TYPE,
                                      TRANSACTION_DEFINITION_ID)
              VALUES (S_TXN_TRANSACTION.NEXTVAL,
                      LTXN_ASSET_CREDIT.CREATE_DATE,
                      LTXN_ASSET_CREDIT.CREATE_DATE_TIME,
                      LTXN_ASSET_CREDIT.CREATE_TERMINAL,
                      LTXN_ASSET_CREDIT.CREATED_BY,
                      LTXN_ASSET_CREDIT.PROCESS_ID,
                      LTXN_ASSET_CREDIT.UPDATE_DATE,
                      LTXN_ASSET_CREDIT.UPDATE_DATE_TIME,
                      LTXN_ASSET_CREDIT.UPDATE_TERMINAL,
                      LTXN_ASSET_CREDIT.UPDATED_BY,
                      LTXN_ASSET_CREDIT.UUID,
                      LTXN_ASSET_CREDIT.VERIFY_BY,
                      LTXN_ASSET_CREDIT.VERIFY_DATE,
                      LTXN_ASSET_CREDIT.VERIFY_DATE_TIME,
                      LTXN_ASSET_CREDIT.VERIFY_TERMINAL,
                      LTXN_ASSET_CREDIT.ACC_MODULE_TYPE,
                      LTXN_ASSET_CREDIT.ACC_NUM,
                      LTXN_ASSET_CREDIT.ACC_TYPE,
                      LTXN_ASSET_CREDIT.AMOUNT,
                      LTXN_ASSET_CREDIT.BRANCH_ID,
                      LTXN_ASSET_CREDIT.BRANCH_PREDEFINED_OPTION,
                      LTXN_ASSET_CREDIT.CURRENCY_CODE,
                      LTXN_ASSET_CREDIT.IS_EXECUTABLE,
                      LTXN_ASSET_CREDIT.INITIATOR_BRANCH_ID,
                      LTXN_ASSET_CREDIT.NARRATION,
                      LTXN_ASSET_CREDIT.TYPE,
                      LIS_EXECUTED,
                      LTXN_ASSET_CREDIT.BATCH_FILE_ID,
                      LTXN_ASSET_CREDIT.TRANSACTION_ROOT_ID,
                      LTXN_ASSET_CREDIT.AMOUNT_TYPE,
                      LTXN_ASSET_CREDIT.TRANSACTION_DEFINITION_ID);
      END LOOP;

      --====================================LIABILITY_DEBIT==================================
      FOR LTXN_LIABILITY_DEBIT
         IN (SELECT *
               FROM TXN_TXN_BALANCE_VALIDATED txn
              WHERE     TXN.BATCH_FILE_ID = LBATCHFILEID
                    AND TXN.ACC_TYPE = 1
                    AND TXN.TYPE = 'DEBIT'
                    AND NOT EXISTS
                               (SELECT 1
                                  FROM TXN_TXN_BALANCE_VALIDATED txnb
                                 WHERE     TXNB.TRANSACTION_ROOT_ID =
                                              txn.TRANSACTION_ROOT_ID
                                       AND TXNB.IS_EXECUTABLE = 0))
      LOOP
         BEGIN
            LRATE :=
               GET_SYSTEM_EXCHANGE_RATE (LTXN_LIABILITY_DEBIT.CURRENCY_CODE);
            LEXCHANGE_RATETYPE :=
               GET_APPCONFIGURATION_VALUE ('SYSTEM_EXCHANGE_RATE_TYPE');
            ALL_TXN_CORE.GETAUTOGENBATCHNO (
               LTXN_LIABILITY_DEBIT.INITIATOR_BRANCH_ID,
               LBATCHNO);

            --            IF LTXN_LIABILITY_DEBIT.ACC_MODULE_TYPE = 'GL'
            --            THEN
            --               LTRDEFID :=
            --                  GET_TXN_DEFINITION_ID (1,
            --                                         2,
            --                                         'GL',
            --                                         'PRINCIPAL');
            --            ELSIF LTXN_LIABILITY_DEBIT.ACC_MODULE_TYPE = 'CASA'
            --            THEN
            --               LTRDEFID :=
            --                  GET_TXN_DEFINITION_ID (1,
            --                                         2,
            --                                         'CASA',
            --                                         'PRINCIPAL');
            --            END IF;

            ALL_TXN_CORE.DO_TRANSACTION (
               LTXN_LIABILITY_DEBIT.CREATE_DATE_TIME,
               LTXN_LIABILITY_DEBIT.CREATE_DATE_TIME,
               LTXN_LIABILITY_DEBIT.AMOUNT,
               LTXN_LIABILITY_DEBIT.AMOUNT,
               LRATE,
               LEXCHANGE_RATETYPE,
               LTXN_LIABILITY_DEBIT.CURRENCY_CODE,
               LTXN_LIABILITY_DEBIT.ACC_NUM,
               LTXN_LIABILITY_DEBIT.NARRATION,
               LBATCHNO,
               LTXN_LIABILITY_DEBIT.ID,
               0,
               NULL,
               LTXN_LIABILITY_DEBIT.ACC_MODULE_TYPE,
               NULL,
               NULL,
               NULL,
               LTXN_LIABILITY_DEBIT.TRANSACTION_DEFINITION_ID,
               LTXN_LIABILITY_DEBIT.BRANCH_ID,
               LTXN_LIABILITY_DEBIT.INITIATOR_BRANCH_ID,
               LTXN_LIABILITY_DEBIT.CREATED_BY,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               LTRECERNO,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL);
            LIS_EXECUTED := 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               COMMON.RAISEEXCEPTION (
                  SQLERRM || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         --               LIS_EXECUTED := 0;
         END;

         INSERT INTO TXN_TRANSACTION (ID,
                                      CREATE_DATE,
                                      CREATE_DATE_TIME,
                                      CREATE_TERMINAL,
                                      CREATED_BY,
                                      PROCESS_ID,
                                      UPDATE_DATE,
                                      UPDATE_DATE_TIME,
                                      UPDATE_TERMINAL,
                                      UPDATED_BY,
                                      UUID,
                                      VERIFY_BY,
                                      VERIFY_DATE,
                                      VERIFY_DATE_TIME,
                                      VERIFY_TERMINAL,
                                      ACC_MODULE_TYPE,
                                      ACC_NUM,
                                      ACC_TYPE,
                                      AMOUNT,
                                      BRANCH_ID,
                                      BRANCH_PREDEFINED_OPTION,
                                      CURRENCY_CODE,
                                      IS_EXECUTABLE,
                                      INITIATOR_BRANCH_ID,
                                      NARRATION,
                                      TYPE,
                                      IS_EXECUTED,
                                      BATCH_FILE_ID,
                                      TRANSACTION_ROOT_ID,
                                      AMOUNT_TYPE,
                                      TRANSACTION_DEFINITION_ID)
              VALUES (S_TXN_TRANSACTION.NEXTVAL,
                      LTXN_LIABILITY_DEBIT.CREATE_DATE,
                      LTXN_LIABILITY_DEBIT.CREATE_DATE_TIME,
                      LTXN_LIABILITY_DEBIT.CREATE_TERMINAL,
                      LTXN_LIABILITY_DEBIT.CREATED_BY,
                      LTXN_LIABILITY_DEBIT.PROCESS_ID,
                      LTXN_LIABILITY_DEBIT.UPDATE_DATE,
                      LTXN_LIABILITY_DEBIT.UPDATE_DATE_TIME,
                      LTXN_LIABILITY_DEBIT.UPDATE_TERMINAL,
                      LTXN_LIABILITY_DEBIT.UPDATED_BY,
                      LTXN_LIABILITY_DEBIT.UUID,
                      LTXN_LIABILITY_DEBIT.VERIFY_BY,
                      LTXN_LIABILITY_DEBIT.VERIFY_DATE,
                      LTXN_LIABILITY_DEBIT.VERIFY_DATE_TIME,
                      LTXN_LIABILITY_DEBIT.VERIFY_TERMINAL,
                      LTXN_LIABILITY_DEBIT.ACC_MODULE_TYPE,
                      LTXN_LIABILITY_DEBIT.ACC_NUM,
                      LTXN_LIABILITY_DEBIT.ACC_TYPE,
                      LTXN_LIABILITY_DEBIT.AMOUNT,
                      LTXN_LIABILITY_DEBIT.BRANCH_ID,
                      LTXN_LIABILITY_DEBIT.BRANCH_PREDEFINED_OPTION,
                      LTXN_LIABILITY_DEBIT.CURRENCY_CODE,
                      LTXN_LIABILITY_DEBIT.IS_EXECUTABLE,
                      LTXN_LIABILITY_DEBIT.INITIATOR_BRANCH_ID,
                      LTXN_LIABILITY_DEBIT.NARRATION,
                      LTXN_LIABILITY_DEBIT.TYPE,
                      LIS_EXECUTED,
                      LTXN_LIABILITY_DEBIT.BATCH_FILE_ID,
                      LTXN_LIABILITY_DEBIT.TRANSACTION_ROOT_ID,
                      LTXN_LIABILITY_DEBIT.AMOUNT_TYPE,
                      LTXN_LIABILITY_DEBIT.TRANSACTION_DEFINITION_ID);
      END LOOP;
   END;
END;
/