package com.mislbd.ababil.transaction.service;

import java.io.InputStream;
import java.io.InputStreamReader;

public interface CsvReaderService {
    InputStreamReader newReader(final InputStream inputStream);
}
