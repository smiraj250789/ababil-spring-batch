package com.mislbd.ababil.transaction.jpa;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
public class TransactionItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String debitNarration;
    private String debitAccountNumber;
    private String creditNarration;
    private String creditAccountNumber;
    private BigDecimal amount;
}
