package com.mislbd.ababil.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Responsibility:
 *
 * @author Rezaul Hasan
 * @since 08-Aug-2018
 */
@SpringBootApplication
public class AbabilBatchTransactionApplication {

  public static void main(String[] args) {
    SpringApplication.run(AbabilBatchTransactionApplication.class, args);
  }
}
