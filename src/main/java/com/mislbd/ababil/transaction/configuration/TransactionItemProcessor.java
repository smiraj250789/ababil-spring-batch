package com.mislbd.ababil.transaction.configuration;

import com.mislbd.ababil.transaction.jpa.Account;
import com.mislbd.ababil.transaction.jpa.AccountRepository;
import com.mislbd.ababil.transaction.jpa.TransactionItem;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Optional;

public class TransactionItemProcessor implements ItemProcessor<TransactionItem, TransactionItem> {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public TransactionItem process(TransactionItem item) throws Exception {
        Optional<Account> account = accountRepository.findByAccountNumber(item.getDebitAccountNumber());
//        if(!account.isPresent()){
//            return null;
//        }
//        if (item.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            return item;
//        }
//        return null;
    }
}
