package com.mislbd.ababil.transaction.configuration;

import com.mislbd.ababil.transaction.jpa.TransactionItem;
import com.mislbd.ababil.transaction.jpa.TransactionItemRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TransactionItemWriter implements ItemWriter<TransactionItem> {

    private final TransactionItemRepository repository;

    public TransactionItemWriter(TransactionItemRepository repository) {
        this.repository = repository;
    }

    @Override
    public void write(List<? extends TransactionItem> list) throws Exception {
        System.out.println("Writer Thread "+Thread.currentThread().getName());
        repository.saveAll(list);
    }
}
